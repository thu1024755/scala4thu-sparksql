import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Created by mark on 04/06/2017.
  */
object YourFirstSparkSQLApp extends App{

  val ss = SparkSession
    .builder()
    .master("local[*]")
    .appName("Spark SQL first example")
    .getOrCreate()

 val src: DataFrame =ss.read.option("header", "true")
  .csv("dataset/small-ratings.csv")
//
 src.createOrReplaceTempView("ratings")
//
//  ss.sql("SELECT movieId,avg(rating) as mean FROM ratings\nGROUP BY movieId ORDER BY mean DESC").show()

  val movieSrc: DataFrame =ss.read.option("header", "true")
    .csv("dataset/movies.csv")

//  movieSrc.createOrReplaceTempView("SELECT movieId,title FROM movies WHERE movieId=10")

  ss.sql("SELECT r.movieId ,m.title ,rating,timestamp\nFROM ratings AS r JOIN movies as m\nWHERE m.movieId=r.movieId\nORDER BY timestamp DESC ").show()

}
